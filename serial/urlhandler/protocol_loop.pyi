from serial.serialutil import (
    PortNotOpenError as PortNotOpenError,
    SerialBase as SerialBase,
    SerialException as SerialException,
    SerialTimeoutException as SerialTimeoutException,
    iterbytes as iterbytes,
    to_bytes as to_bytes,
)
from typing import Any

LOGGER_LEVELS: Any

class Serial(SerialBase):
    BAUDRATES: Any
    buffer_size: int
    queue: Any
    logger: Any
    def __init__(self, *args, **kwargs) -> None: ...
    is_open: bool
    def open(self) -> None: ...
    def close(self) -> None: ...
    def from_url(self, url) -> None: ...
    @property
    def in_waiting(self): ...
    def read(self, size: int = ...): ...
    def cancel_read(self) -> None: ...
    def cancel_write(self) -> None: ...
    def write(self, data): ...
    def reset_input_buffer(self) -> None: ...
    def reset_output_buffer(self) -> None: ...
    @property
    def out_waiting(self): ...
    @property
    def cts(self): ...
    @property
    def dsr(self): ...
    @property
    def ri(self): ...
    @property
    def cd(self): ...
