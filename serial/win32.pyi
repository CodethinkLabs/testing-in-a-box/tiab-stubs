from ctypes import Structure, Union, c_int64, c_ulong, c_void_p
from typing import Any

ULONG_PTR = c_int64
ULONG_PTR = c_ulong

class _SECURITY_ATTRIBUTES(Structure): ...

LPSECURITY_ATTRIBUTES: Any
CreateEventW: Any
CreateEvent = CreateEventA
CreateFile = CreateFileA
CreateEvent = CreateEventW
CreateFileW: Any
CreateFile = CreateFileW

class _OVERLAPPED(Structure): ...

OVERLAPPED: Any

class _COMSTAT(Structure): ...

COMSTAT: Any

class _DCB(Structure): ...

DCB: Any

class _COMMTIMEOUTS(Structure): ...

COMMTIMEOUTS: Any
GetLastError: Any
LPOVERLAPPED: Any
LPDWORD: Any
GetOverlappedResult: Any
ResetEvent: Any
LPCVOID = c_void_p
WriteFile: Any
LPVOID = c_void_p
ReadFile: Any
CloseHandle: Any
ClearCommBreak: Any
LPCOMSTAT: Any
ClearCommError: Any
SetupComm: Any
EscapeCommFunction: Any
GetCommModemStatus: Any
LPDCB: Any
GetCommState: Any
LPCOMMTIMEOUTS: Any
GetCommTimeouts: Any
PurgeComm: Any
SetCommBreak: Any
SetCommMask: Any
SetCommState: Any
SetCommTimeouts: Any
ONESTOPBIT: int
TWOSTOPBITS: int
NOPARITY: int
ODDPARITY: int
EVENPARITY: int
RTS_CONTROL_HANDSHAKE: int
RTS_CONTROL_ENABLE: int
DTR_CONTROL_HANDSHAKE: int
DTR_CONTROL_ENABLE: int
MS_DSR_ON: int
EV_RING: int
EV_PERR: int
EV_ERR: int
SETXOFF: int
EV_RXCHAR: int
GENERIC_WRITE: int
PURGE_TXCLEAR: int
FILE_FLAG_OVERLAPPED: int
EV_DSR: int
MAXDWORD: int
EV_RLSD: int
ERROR_IO_PENDING: int
MS_CTS_ON: int
EV_EVENT1: int
EV_RX80FULL: int
PURGE_RXABORT: int
FILE_ATTRIBUTE_NORMAL: int
PURGE_TXABORT: int
SETXON: int
OPEN_EXISTING: int
MS_RING_ON: int
EV_TXEMPTY: int
EV_RXFLAG: int
MS_RLSD_ON: int
GENERIC_READ: int
EV_EVENT2: int
EV_CTS: int
EV_BREAK: int
PURGE_RXCLEAR: int

class N11_OVERLAPPED4DOLLAR_48E(Union): ...
class N11_OVERLAPPED4DOLLAR_484DOLLAR_49E(Structure): ...

PVOID = c_void_p
