import usb.core as core
from _typeshed import Incomplete

USBError = core.USBError
ENDPOINT_HALT: int
FUNCTION_SUSPEND: int
DEVICE_REMOTE_WAKEUP: int
U1_ENABLE: int
U2_ENABLE: int
LTM_ENABLE: int

def get_status(dev, recipient: Incomplete | None = ...): ...
def clear_feature(dev, feature, recipient: Incomplete | None = ...) -> None: ...
def set_feature(dev, feature, recipient: Incomplete | None = ...) -> None: ...
def get_descriptor(dev, desc_size, desc_type, desc_index, wIndex: int = ...): ...
def set_descriptor(
    dev, desc, desc_type, desc_index, wIndex: Incomplete | None = ...
) -> None: ...
def get_configuration(dev): ...
def set_configuration(dev, bConfigurationNumber) -> None: ...
def get_interface(dev, bInterfaceNumber): ...
def set_interface(dev, bInterfaceNumber, bAlternateSetting) -> None: ...
