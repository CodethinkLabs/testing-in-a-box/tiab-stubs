class _AutoFinalizedObjectBase:
    def __new__(cls, *args, **kwargs): ...
    def finalize(self) -> None: ...
    def __del__(self) -> None: ...

class AutoFinalizedObject(_AutoFinalizedObjectBase):
    def __new__(cls, *args, **kwargs): ...
    def finalize(self) -> None: ...
