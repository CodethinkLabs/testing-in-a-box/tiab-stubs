from _typeshed import Incomplete

descriptors: Incomplete
device_classes: Incomplete
interface_classes: Incomplete
ep_attributes: Incomplete
MAX_POWER_UNITS_USB2p0: int
MAX_POWER_UNITS_USB_SUPERSPEED: int
