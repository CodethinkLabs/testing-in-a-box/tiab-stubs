from _typeshed import Incomplete

__description__: str

class FtdiLogger:
    log: Incomplete
    @classmethod
    def set_formatter(cls, formatter) -> None: ...
    @classmethod
    def get_level(cls): ...
    @classmethod
    def set_level(cls, level) -> None: ...
