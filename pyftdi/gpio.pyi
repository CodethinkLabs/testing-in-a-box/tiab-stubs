from .ftdi import Ftdi as Ftdi, FtdiError as FtdiError
from .misc import is_iterable as is_iterable
from _typeshed import Incomplete
from typing import Iterable, Optional, Tuple, Union

class GpioException(FtdiError): ...
class GpioPort: ...

class GpioBaseController(GpioPort):
    def __init__(self) -> None: ...
    @property
    def ftdi(self) -> Ftdi: ...
    @property
    def is_connected(self) -> bool: ...
    def configure(self, url: str, direction: int = ..., **kwargs) -> int: ...
    def close(self, freeze: bool = ...) -> None: ...
    def get_gpio(self) -> GpioPort: ...
    @property
    def direction(self) -> int: ...
    @property
    def pins(self) -> int: ...
    @property
    def all_pins(self) -> int: ...
    @property
    def width(self) -> int: ...
    @property
    def frequency(self) -> float: ...
    def set_frequency(self, frequency: Union[int, float]) -> None: ...
    def set_direction(self, pins: int, direction: int) -> None: ...

class GpioAsyncController(GpioBaseController):
    def read(
        self, readlen: int = ..., peek: Optional[bool] = ..., noflush: bool = ...
    ) -> Union[int, bytes]: ...
    def write(self, out: Union[bytes, bytearray, int]) -> None: ...
    def set_frequency(self, frequency: Union[int, float]) -> None: ...
    open_from_url: Incomplete
    read_port = read
    write_port = write

GpioController = GpioAsyncController

class GpioSyncController(GpioBaseController):
    def exchange(self, out: Union[bytes, bytearray]) -> bytes: ...
    def set_frequency(self, frequency: Union[int, float]) -> None: ...

class GpioMpsseController(GpioBaseController):
    MPSSE_PAYLOAD_MAX_LENGTH: int
    def read(
        self, readlen: int = ..., peek: Optional[bool] = ...
    ) -> Union[int, bytes, Tuple[int]]: ...
    def write(self, out: Union[bytes, bytearray, Iterable[int], int]) -> None: ...
    def set_frequency(self, frequency: Union[int, float]) -> None: ...
