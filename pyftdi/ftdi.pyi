from .misc import to_bool as to_bool
from .usbtools import UsbDeviceDescriptor as UsbDeviceDescriptor, UsbTools as UsbTools
from _typeshed import Incomplete
from enum import IntEnum
from typing import Callable, List, Optional, Sequence, TextIO, Tuple, Union
from usb.core import Device as UsbDevice

class FtdiError(IOError): ...
class FtdiFeatureError(FtdiError): ...
class FtdiMpsseError(FtdiFeatureError): ...
class FtdiEepromError(FtdiError): ...

class Ftdi:
    SCHEME: str
    FTDI_VENDOR: int
    VENDOR_IDS: Incomplete
    PRODUCT_IDS: Incomplete
    DEFAULT_VENDOR = FTDI_VENDOR
    DEVICE_NAMES: Incomplete
    FIFO_SIZES: Incomplete

    class BitMode(IntEnum):
        RESET: int
        BITBANG: int
        MPSSE: int
        SYNCBB: int
        MCU: int
        OPTO: int
        CBUS: int
        SYNCFF: int
    WRITE_BYTES_PVE_MSB: int
    WRITE_BYTES_NVE_MSB: int
    WRITE_BITS_PVE_MSB: int
    WRITE_BITS_NVE_MSB: int
    WRITE_BYTES_PVE_LSB: int
    WRITE_BYTES_NVE_LSB: int
    WRITE_BITS_PVE_LSB: int
    WRITE_BITS_NVE_LSB: int
    READ_BYTES_PVE_MSB: int
    READ_BYTES_NVE_MSB: int
    READ_BITS_PVE_MSB: int
    READ_BITS_NVE_MSB: int
    READ_BYTES_PVE_LSB: int
    READ_BYTES_NVE_LSB: int
    READ_BITS_PVE_LSB: int
    READ_BITS_NVE_LSB: int
    RW_BYTES_PVE_NVE_MSB: int
    RW_BYTES_NVE_PVE_MSB: int
    RW_BITS_PVE_NVE_MSB: int
    RW_BITS_NVE_PVE_MSB: int
    RW_BYTES_PVE_NVE_LSB: int
    RW_BYTES_NVE_PVE_LSB: int
    RW_BITS_PVE_NVE_LSB: int
    RW_BITS_NVE_PVE_LSB: int
    WRITE_BITS_TMS_PVE: int
    WRITE_BITS_TMS_NVE: int
    RW_BITS_TMS_PVE_PVE: int
    RW_BITS_TMS_PVE_NVE: int
    RW_BITS_TMS_NVE_PVE: int
    RW_BITS_TMS_NVE_NVE: int
    SEND_IMMEDIATE: int
    WAIT_ON_HIGH: int
    WAIT_ON_LOW: int
    READ_SHORT: int
    READ_EXTENDED: int
    WRITE_SHORT: int
    WRITE_EXTENDED: int
    DISABLE_CLK_DIV5: int
    ENABLE_CLK_DIV5: int
    MODEM_CTS: Incomplete
    MODEM_DSR: Incomplete
    MODEM_RI: Incomplete
    MODEM_RLSD: Incomplete
    MODEM_DR: Incomplete
    MODEM_OE: Incomplete
    MODEM_PE: Incomplete
    MODEM_FE: Incomplete
    MODEM_BI: Incomplete
    MODEM_THRE: Incomplete
    MODEM_TEMT: Incomplete
    MODEM_RCVE: Incomplete
    SET_BITS_LOW: int
    SET_BITS_HIGH: int
    GET_BITS_LOW: int
    GET_BITS_HIGH: int
    LOOPBACK_START: int
    LOOPBACK_END: int
    SET_TCK_DIVISOR: int
    ENABLE_CLK_3PHASE: int
    DISABLE_CLK_3PHASE: int
    CLK_BITS_NO_DATA: int
    CLK_BYTES_NO_DATA: int
    CLK_WAIT_ON_HIGH: int
    CLK_WAIT_ON_LOW: int
    ENABLE_CLK_ADAPTIVE: int
    DISABLE_CLK_ADAPTIVE: int
    CLK_COUNT_WAIT_ON_HIGH: int
    CLK_COUNT_WAIT_ON_LOW: int
    DRIVE_ZERO: int
    REQ_OUT: Incomplete
    REQ_IN: Incomplete
    SIO_REQ_RESET: int
    SIO_REQ_SET_MODEM_CTRL: int
    SIO_REQ_SET_FLOW_CTRL: int
    SIO_REQ_SET_BAUDRATE: int
    SIO_REQ_SET_DATA: int
    SIO_REQ_POLL_MODEM_STATUS: int
    SIO_REQ_SET_EVENT_CHAR: int
    SIO_REQ_SET_ERROR_CHAR: int
    SIO_REQ_SET_LATENCY_TIMER: int
    SIO_REQ_GET_LATENCY_TIMER: int
    SIO_REQ_SET_BITMODE: int
    SIO_REQ_READ_PINS: int
    SIO_REQ_EEPROM: int
    SIO_REQ_READ_EEPROM: Incomplete
    SIO_REQ_WRITE_EEPROM: Incomplete
    SIO_REQ_ERASE_EEPROM: Incomplete
    SIO_RESET_SIO: int
    SIO_RESET_PURGE_RX: int
    SIO_RESET_PURGE_TX: int
    SIO_DISABLE_FLOW_CTRL: int
    SIO_RTS_CTS_HS: Incomplete
    SIO_DTR_DSR_HS: Incomplete
    SIO_XON_XOFF_HS: Incomplete
    SIO_SET_DTR_MASK: int
    SIO_SET_DTR_HIGH: Incomplete
    SIO_SET_DTR_LOW: Incomplete
    SIO_SET_RTS_MASK: int
    SIO_SET_RTS_HIGH: Incomplete
    SIO_SET_RTS_LOW: Incomplete
    PARITY_NONE: Incomplete
    PARITY_ODD: Incomplete
    PARITY_EVEN: Incomplete
    PARITY_MARK: Incomplete
    PARITY_SPACE: Incomplete
    STOP_BIT_1: Incomplete
    STOP_BIT_15: Incomplete
    STOP_BIT_2: Incomplete
    BITS_7: Incomplete
    BITS_8: Incomplete
    BREAK_OFF: Incomplete
    BREAK_ON: Incomplete
    MODEM_STATUS: Incomplete
    ERROR_BITS: Incomplete
    TX_EMPTY_BITS: int
    BUS_CLOCK_BASE: float
    BUS_CLOCK_HIGH: float
    BAUDRATE_REF_BASE: Incomplete
    BAUDRATE_REF_HIGH: Incomplete
    BITBANG_BAUDRATE_RATIO_BASE: int
    BITBANG_BAUDRATE_RATIO_HIGH: int
    BAUDRATE_TOLERANCE: float
    FRAC_DIV_CODE: Incomplete
    LATENCY_MIN: int
    LATENCY_MAX: int
    LATENCY_EEPROM_FT232R: int
    EXT_EEPROM_SIZES: Incomplete
    INT_EEPROMS: Incomplete
    log: Incomplete
    def __init__(self) -> None: ...
    @classmethod
    def create_from_url(cls, url: str) -> Ftdi: ...
    @classmethod
    def list_devices(
        cls, url: Optional[str] = ...
    ) -> List[Tuple[UsbDeviceDescriptor, int]]: ...
    @classmethod
    def show_devices(
        cls, url: Optional[str] = ..., out: Optional[TextIO] = ...
    ) -> None: ...
    @classmethod
    def get_identifiers(cls, url: str) -> Tuple[UsbDeviceDescriptor, int]: ...
    @classmethod
    def get_device(cls, url: str) -> UsbDevice: ...
    @classmethod
    def add_custom_vendor(cls, vid: int, vidname: str = ...) -> None: ...
    @classmethod
    def add_custom_product(cls, vid: int, pid: int, pidname: str = ...) -> None: ...
    @classmethod
    def decode_modem_status(
        cls, value: bytes, error_only: bool = ...
    ) -> Tuple[str, ...]: ...
    @staticmethod
    def find_all(
        vps: Sequence[Tuple[int, int]], nocache: bool = ...
    ) -> List[Tuple[UsbDeviceDescriptor, int]]: ...
    @property
    def is_connected(self) -> bool: ...
    def open_from_url(self, url: str) -> None: ...
    def open(
        self,
        vendor: int,
        product: int,
        bus: Optional[int] = ...,
        address: Optional[int] = ...,
        index: int = ...,
        serial: Optional[str] = ...,
        interface: int = ...,
    ) -> None: ...
    def open_from_device(self, device: UsbDevice, interface: int = ...) -> None: ...
    def close(self, freeze: bool = ...) -> None: ...
    def reset(self, usb_reset: bool = ...) -> None: ...
    def open_mpsse_from_url(
        self,
        url: str,
        direction: int = ...,
        initial: int = ...,
        frequency: float = ...,
        latency: int = ...,
        debug: bool = ...,
    ) -> float: ...
    def open_mpsse(
        self,
        vendor: int,
        product: int,
        bus: Optional[int] = ...,
        address: Optional[int] = ...,
        index: int = ...,
        serial: Optional[str] = ...,
        interface: int = ...,
        direction: int = ...,
        initial: int = ...,
        frequency: float = ...,
        latency: int = ...,
        debug: bool = ...,
    ) -> float: ...
    def open_mpsse_from_device(
        self,
        device: UsbDevice,
        interface: int = ...,
        direction: int = ...,
        initial: int = ...,
        frequency: float = ...,
        latency: int = ...,
        tracer: bool = ...,
        debug: bool = ...,
    ) -> float: ...
    def open_bitbang_from_url(
        self,
        url: str,
        direction: int = ...,
        latency: int = ...,
        baudrate: int = ...,
        sync: bool = ...,
    ) -> float: ...
    def open_bitbang(
        self,
        vendor: int,
        product: int,
        bus: Optional[int] = ...,
        address: Optional[int] = ...,
        index: int = ...,
        serial: Optional[str] = ...,
        interface: int = ...,
        direction: int = ...,
        latency: int = ...,
        baudrate: int = ...,
        sync: bool = ...,
    ) -> float: ...
    def open_bitbang_from_device(
        self,
        device: UsbDevice,
        interface: int = ...,
        direction: int = ...,
        latency: int = ...,
        baudrate: int = ...,
        sync: bool = ...,
    ) -> int: ...
    @property
    def usb_path(self) -> Tuple[int, int, int]: ...
    @property
    def device_version(self) -> int: ...
    @property
    def ic_name(self) -> str: ...
    @property
    def device_port_count(self) -> int: ...
    @property
    def port_index(self) -> int: ...
    @property
    def port_width(self) -> int: ...
    @property
    def has_mpsse(self) -> bool: ...
    @property
    def has_wide_port(self) -> bool: ...
    @property
    def has_cbus(self) -> bool: ...
    @property
    def has_drivezero(self) -> bool: ...
    @property
    def is_legacy(self) -> bool: ...
    @property
    def is_H_series(self) -> bool: ...
    @property
    def is_mpsse(self) -> bool: ...
    def is_mpsse_interface(self, interface: int) -> bool: ...
    @property
    def is_bitbang_enabled(self) -> bool: ...
    bitbang_enabled = is_bitbang_enabled
    @property
    def is_eeprom_internal(self) -> bool: ...
    @property
    def max_eeprom_size(self) -> int: ...
    @property
    def frequency_max(self) -> float: ...
    @property
    def fifo_sizes(self) -> Tuple[int, int]: ...
    @property
    def mpsse_bit_delay(self) -> float: ...
    @property
    def baudrate(self) -> int: ...
    @property
    def usb_dev(self) -> UsbDevice: ...
    def set_baudrate(self, baudrate: int, constrain: bool = ...) -> int: ...
    def set_frequency(self, frequency: float) -> float: ...
    def purge_rx_buffer(self) -> None: ...
    def purge_tx_buffer(self) -> None: ...
    def purge_buffers(self) -> None: ...
    def write_data_set_chunksize(self, chunksize: int = ...) -> None: ...
    def write_data_get_chunksize(self) -> int: ...
    def read_data_set_chunksize(self, chunksize: int = ...) -> None: ...
    def read_data_get_chunksize(self) -> int: ...
    def set_bitmode(self, bitmask: int, mode: Ftdi.BitMode) -> None: ...
    def read_pins(self) -> int: ...
    def set_cbus_direction(self, mask: int, direction: int) -> None: ...
    def get_cbus_gpio(self) -> int: ...
    def set_cbus_gpio(self, pins: int) -> None: ...
    def set_latency_timer(self, latency: int): ...
    def get_latency_timer(self) -> int: ...
    def poll_modem_status(self) -> int: ...
    def modem_status(self) -> Tuple[str, ...]: ...
    def set_flowctrl(self, flowctrl: str) -> None: ...
    def set_dtr(self, state: bool) -> None: ...
    def set_rts(self, state: bool) -> None: ...
    def set_dtr_rts(self, dtr: bool, rts: bool) -> None: ...
    def set_break(self, break_: bool) -> None: ...
    def set_event_char(self, eventch: int, enable: bool) -> None: ...
    def set_error_char(self, errorch: int, enable: bool) -> None: ...
    def set_line_property(
        self, bits: int, stopbit: Union[int, float], parity: str, break_: bool = ...
    ) -> None: ...
    def enable_adaptive_clock(self, enable: bool = ...) -> None: ...
    def enable_3phase_clock(self, enable: bool = ...) -> None: ...
    def enable_drivezero_mode(self, lines: int) -> None: ...
    def enable_loopback_mode(self, loopback: bool = ...) -> None: ...
    def calc_eeprom_checksum(self, data: Union[bytes, bytearray]) -> int: ...
    def read_eeprom(
        self,
        addr: int = ...,
        length: Optional[int] = ...,
        eeprom_size: Optional[int] = ...,
    ) -> bytes: ...
    def write_eeprom(
        self,
        addr: int,
        data: Union[bytes, bytearray],
        eeprom_size: Optional[int] = ...,
        dry_run: bool = ...,
    ) -> None: ...
    def overwrite_eeprom(
        self, data: Union[bytes, bytearray], dry_run: bool = ...
    ) -> None: ...
    def write_data(self, data: Union[bytes, bytearray]) -> int: ...
    def read_data_bytes(
        self,
        size: int,
        attempt: int = ...,
        request_gen: Optional[Callable[[int], bytes]] = ...,
    ) -> bytes: ...
    def read_data(self, size: int) -> bytes: ...
    def get_cts(self) -> bool: ...
    def get_dsr(self) -> bool: ...
    def get_ri(self) -> bool: ...
    def get_cd(self) -> bool: ...
    def set_dynamic_latency(self, lmin: int, lmax: int, threshold: int) -> None: ...
    def validate_mpsse(self) -> None: ...
    @classmethod
    def get_error_string(cls) -> str: ...
    timeouts: Incomplete
