from .misc import to_int as to_int
from _typeshed import Incomplete
from typing import Dict, List, Optional, Sequence, TextIO, Tuple, Type, Union
from urllib.parse import SplitResult as SplitResult
from usb.backend import IBackend as IBackend
from usb.core import Device as UsbDevice

UsbDeviceDescriptor: Incomplete
UsbDeviceKey = Union[Tuple[int, int, int, int], Tuple[int, int]]

class UsbToolsError(Exception): ...

class UsbTools:
    BACKENDS: Incomplete
    Lock: Incomplete
    Devices: Incomplete
    UsbDevices: Incomplete
    UsbApi: Incomplete
    @classmethod
    def find_all(
        cls, vps: Sequence[Tuple[int, int]], nocache: bool = ...
    ) -> List[Tuple[UsbDeviceDescriptor, int]]: ...
    @classmethod
    def flush_cache(cls) -> None: ...
    @classmethod
    def get_device(cls, devdesc: UsbDeviceDescriptor) -> UsbDevice: ...
    @classmethod
    def release_device(cls, usb_dev: UsbDevice): ...
    @classmethod
    def release_all_devices(cls, devclass: Optional[type] = ...) -> int: ...
    @classmethod
    def list_devices(
        cls,
        urlstr: str,
        vdict: Dict[str, int],
        pdict: Dict[int, Dict[str, int]],
        default_vendor: int,
    ) -> List[Tuple[UsbDeviceDescriptor, int]]: ...
    @classmethod
    def parse_url(
        cls,
        urlstr: str,
        scheme: str,
        vdict: Dict[str, int],
        pdict: Dict[int, Dict[str, int]],
        default_vendor: int,
    ) -> Tuple[UsbDeviceDescriptor, int]: ...
    @classmethod
    def enumerate_candidates(
        cls,
        urlparts: SplitResult,
        vdict: Dict[str, int],
        pdict: Dict[int, Dict[str, int]],
        default_vendor: int,
    ) -> Tuple[List[Tuple[UsbDeviceDescriptor, int]], Optional[int]]: ...
    @classmethod
    def show_devices(
        cls,
        scheme: str,
        vdict: Dict[str, int],
        pdict: Dict[int, Dict[str, int]],
        devdescs: Sequence[Tuple[UsbDeviceDescriptor, int]],
        out: Optional[TextIO] = ...,
    ): ...
    @classmethod
    def build_dev_strings(
        cls,
        scheme: str,
        vdict: Dict[str, int],
        pdict: Dict[int, Dict[str, int]],
        devdescs: Sequence[Tuple[UsbDeviceDescriptor, int]],
    ) -> List[Tuple[str, str]]: ...
    @classmethod
    def get_string(cls, device: UsbDevice, stridx: int) -> str: ...
    @classmethod
    def find_backend(cls) -> IBackend: ...
