from _typeshed import Incomplete
from serial import serial_for_url as serial4url

version: Incomplete
serial_for_url = serial4url

def touch() -> None: ...
