from strictyaml.compound import FixedSeq as FixedSeq, Map as Map
from strictyaml.exceptions import YAMLSerializationError as YAMLSerializationError
from strictyaml.ruamel.comments import (
    CommentedMap as CommentedMap,
    CommentedSeq as CommentedSeq,
)
from strictyaml.scalar import (
    Bool as Bool,
    EmptyDict as EmptyDict,
    EmptyList as EmptyList,
    Float as Float,
    Int as Int,
    Str as Str,
)
from strictyaml.validators import Validator as Validator

def schema_from_document(document): ...
def schema_from_data(data, allow_empty): ...

class Any(Validator):
    def validate(self, chunk): ...
    def to_yaml(self, data, allow_empty: bool = ...): ...
    @property
    def key_validator(self): ...
