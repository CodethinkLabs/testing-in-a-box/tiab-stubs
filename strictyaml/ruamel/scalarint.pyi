from .compat import no_limit_int
from typing import Any

class ScalarInt(no_limit_int):
    def __new__(cls, *args: Any, **kw: Any) -> Any: ...
    def __iadd__(self, a: Any) -> Any: ...
    def __ifloordiv__(self, a: Any) -> Any: ...
    def __imul__(self, a: Any) -> Any: ...
    def __ipow__(self, a: Any) -> Any: ...
    def __isub__(self, a: Any) -> Any: ...
    @property
    def anchor(self) -> Any: ...
    def yaml_anchor(self, any: bool = ...) -> Any: ...
    def yaml_set_anchor(self, value: Any, always_dump: bool = ...) -> None: ...

class BinaryInt(ScalarInt):
    def __new__(
        cls, value: Any, width: Any = ..., underscore: Any = ..., anchor: Any = ...
    ) -> Any: ...

class OctalInt(ScalarInt):
    def __new__(
        cls, value: Any, width: Any = ..., underscore: Any = ..., anchor: Any = ...
    ) -> Any: ...

class HexInt(ScalarInt):
    def __new__(
        cls, value: Any, width: Any = ..., underscore: Any = ..., anchor: Any = ...
    ) -> Any: ...

class HexCapsInt(ScalarInt):
    def __new__(
        cls, value: Any, width: Any = ..., underscore: Any = ..., anchor: Any = ...
    ) -> Any: ...

class DecimalInt(ScalarInt):
    def __new__(
        cls, value: Any, width: Any = ..., underscore: Any = ..., anchor: Any = ...
    ) -> Any: ...
