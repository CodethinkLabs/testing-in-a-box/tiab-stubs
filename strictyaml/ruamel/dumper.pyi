from strictyaml.ruamel.compat import StreamType
from strictyaml.ruamel.emitter import Emitter
from strictyaml.ruamel.representer import (
    BaseRepresenter,
    Representer,
    RoundTripRepresenter,
    SafeRepresenter,
)
from strictyaml.ruamel.resolver import BaseResolver, Resolver, VersionedResolver
from strictyaml.ruamel.serializer import Serializer
from typing import Any, Optional

class BaseDumper(Emitter, Serializer, BaseRepresenter, BaseResolver):
    def __init__(
        self,
        stream: StreamType,
        default_style: Any = ...,
        default_flow_style: Any = ...,
        canonical: Optional[bool] = ...,
        indent: Optional[int] = ...,
        width: Optional[int] = ...,
        allow_unicode: Optional[bool] = ...,
        line_break: Any = ...,
        encoding: Any = ...,
        explicit_start: Optional[bool] = ...,
        explicit_end: Optional[bool] = ...,
        version: Any = ...,
        tags: Any = ...,
        block_seq_indent: Any = ...,
        top_level_colon_align: Any = ...,
        prefix_colon: Any = ...,
    ) -> None: ...

class SafeDumper(Emitter, Serializer, SafeRepresenter, Resolver):
    def __init__(
        self,
        stream: StreamType,
        default_style: Any = ...,
        default_flow_style: Any = ...,
        canonical: Optional[bool] = ...,
        indent: Optional[int] = ...,
        width: Optional[int] = ...,
        allow_unicode: Optional[bool] = ...,
        line_break: Any = ...,
        encoding: Any = ...,
        explicit_start: Optional[bool] = ...,
        explicit_end: Optional[bool] = ...,
        version: Any = ...,
        tags: Any = ...,
        block_seq_indent: Any = ...,
        top_level_colon_align: Any = ...,
        prefix_colon: Any = ...,
    ) -> None: ...

class Dumper(Emitter, Serializer, Representer, Resolver):
    def __init__(
        self,
        stream: StreamType,
        default_style: Any = ...,
        default_flow_style: Any = ...,
        canonical: Optional[bool] = ...,
        indent: Optional[int] = ...,
        width: Optional[int] = ...,
        allow_unicode: Optional[bool] = ...,
        line_break: Any = ...,
        encoding: Any = ...,
        explicit_start: Optional[bool] = ...,
        explicit_end: Optional[bool] = ...,
        version: Any = ...,
        tags: Any = ...,
        block_seq_indent: Any = ...,
        top_level_colon_align: Any = ...,
        prefix_colon: Any = ...,
    ) -> None: ...

class RoundTripDumper(Emitter, Serializer, RoundTripRepresenter, VersionedResolver):
    def __init__(
        self,
        stream: StreamType,
        default_style: Any = ...,
        default_flow_style: Optional[bool] = ...,
        canonical: Optional[int] = ...,
        indent: Optional[int] = ...,
        width: Optional[int] = ...,
        allow_unicode: Optional[bool] = ...,
        line_break: Any = ...,
        encoding: Any = ...,
        explicit_start: Optional[bool] = ...,
        explicit_end: Optional[bool] = ...,
        version: Any = ...,
        tags: Any = ...,
        block_seq_indent: Any = ...,
        top_level_colon_align: Any = ...,
        prefix_colon: Any = ...,
    ) -> None: ...
