from strictyaml.ruamel.tokens import *
from strictyaml.ruamel.events import *
from strictyaml.ruamel.nodes import *
from _typeshed import Incomplete
from pathlib import Path
from strictyaml.ruamel.compat import (
    BytesIO as BytesIO,
    PY3 as PY3,
    StreamTextType as StreamTextType,
    StreamType as StreamType,
    StringIO as StringIO,
    VersionType as VersionType,
    nprint as nprint,
    with_metaclass as with_metaclass,
)
from strictyaml.ruamel.constructor import (
    BaseConstructor as BaseConstructor,
    Constructor as Constructor,
    RoundTripConstructor as RoundTripConstructor,
    SafeConstructor as SafeConstructor,
)
from strictyaml.ruamel.dumper import (
    BaseDumper as BaseDumper,
    Dumper as Dumper,
    RoundTripDumper as RoundTripDumper,
    SafeDumper as SafeDumper,
)
from strictyaml.ruamel.error import (
    UnsafeLoaderWarning as UnsafeLoaderWarning,
    YAMLError as YAMLError,
)
from strictyaml.ruamel.loader import (
    BaseLoader as BaseLoader,
    Loader as Loader,
    RoundTripLoader as RoundTripLoader,
    SafeLoader as SafeLoader,
)
from strictyaml.ruamel.representer import (
    BaseRepresenter as BaseRepresenter,
    Representer as Representer,
    RoundTripRepresenter as RoundTripRepresenter,
    SafeRepresenter as SafeRepresenter,
)
from strictyaml.ruamel.resolver import (
    Resolver as Resolver,
    VersionedResolver as VersionedResolver,
)
from typing import Any, Optional, Text, Union

enforce: Incomplete

class YAML:
    typ: Incomplete
    pure: Incomplete
    plug_ins: Incomplete
    Resolver: Incomplete
    allow_unicode: bool
    Reader: Incomplete
    Representer: Incomplete
    Constructor: Incomplete
    Scanner: Incomplete
    Serializer: Incomplete
    default_flow_style: Incomplete
    Emitter: Incomplete
    Parser: Incomplete
    Composer: Incomplete
    stream: Incomplete
    canonical: Incomplete
    old_indent: Incomplete
    width: Incomplete
    line_break: Incomplete
    map_indent: Incomplete
    sequence_indent: Incomplete
    sequence_dash_offset: int
    compact_seq_seq: Incomplete
    compact_seq_map: Incomplete
    sort_base_mapping_type_on_output: Incomplete
    top_level_colon_align: Incomplete
    prefix_colon: Incomplete
    version: Incomplete
    preserve_quotes: Incomplete
    allow_duplicate_keys: bool
    encoding: str
    explicit_start: Incomplete
    explicit_end: Incomplete
    tags: Incomplete
    default_style: Incomplete
    top_level_block_style_scalar_no_indent_error_1_1: bool
    scalar_after_indicator: Incomplete
    brace_single_entry_mapping_in_flow_sequence: bool
    def __init__(
        self,
        _kw: Any = ...,
        typ: Optional[Text] = ...,
        pure: Any = ...,
        output: Any = ...,
        plug_ins: Any = ...,
    ) -> None: ...
    @property
    def reader(self) -> Any: ...
    @property
    def scanner(self) -> Any: ...
    @property
    def parser(self) -> Any: ...
    @property
    def composer(self) -> Any: ...
    @property
    def constructor(self) -> Any: ...
    @property
    def resolver(self) -> Any: ...
    @property
    def emitter(self) -> Any: ...
    @property
    def serializer(self) -> Any: ...
    @property
    def representer(self) -> Any: ...
    def load(self, stream: Union[Path, StreamTextType]) -> Any: ...
    def load_all(self, stream: Union[Path, StreamTextType], _kw: Any = ...) -> Any: ...
    def get_constructor_parser(self, stream: StreamTextType) -> Any: ...
    def dump(
        self,
        data: Any,
        stream: Union[Path, StreamType] = ...,
        _kw: Any = ...,
        transform: Any = ...,
    ) -> Any: ...
    def dump_all(
        self,
        documents: Any,
        stream: Union[Path, StreamType],
        _kw: Any = ...,
        transform: Any = ...,
    ) -> Any: ...
    def Xdump_all(
        self,
        documents: Any,
        stream: Union[Path, StreamType],
        _kw: Any = ...,
        transform: Any = ...,
    ) -> Any: ...
    def get_serializer_representer_emitter(
        self, stream: StreamType, tlca: Any
    ) -> Any: ...
    def map(self, **kw: Any) -> Any: ...
    def seq(self, *args: Any) -> Any: ...
    def official_plug_ins(self) -> Any: ...
    def register_class(self, cls: Any) -> Any: ...
    def parse(self, stream: StreamTextType) -> Any: ...
    def __enter__(self) -> Any: ...
    def __exit__(self, typ: Any, value: Any, traceback: Any) -> None: ...
    @property
    def indent(self) -> Any: ...
    @indent.setter
    def indent(self, val: Any) -> None: ...
    @property
    def block_seq_indent(self) -> Any: ...
    @block_seq_indent.setter
    def block_seq_indent(self, val: Any) -> None: ...
    def compact(self, seq_seq: Any = ..., seq_map: Any = ...) -> None: ...

class YAMLContextManager:
    def __init__(self, yaml: Any, transform: Any = ...) -> None: ...
    def teardown_output(self) -> None: ...
    def init_output(self, first_data: Any) -> None: ...
    def dump(self, data: Any) -> None: ...

def yaml_object(yml: Any) -> Any: ...
def scan(stream: StreamTextType, Loader: Any = ...) -> Any: ...
def parse(stream: StreamTextType, Loader: Any = ...) -> Any: ...
def compose(stream: StreamTextType, Loader: Any = ...) -> Any: ...
def compose_all(stream: StreamTextType, Loader: Any = ...) -> Any: ...
def load(
    stream: StreamTextType,
    Loader: Any = ...,
    version: Optional[VersionType] = ...,
    preserve_quotes: Any = ...,
) -> Any: ...
def load_all(
    stream: Optional[StreamTextType],
    Loader: Any = ...,
    version: Optional[VersionType] = ...,
    preserve_quotes: Optional[bool] = ...,
) -> Any: ...
def safe_load(stream: StreamTextType, version: Optional[VersionType] = ...) -> Any: ...
def safe_load_all(
    stream: StreamTextType, version: Optional[VersionType] = ...
) -> Any: ...
def round_trip_load(
    stream: StreamTextType,
    version: Optional[VersionType] = ...,
    preserve_quotes: Optional[bool] = ...,
) -> Any: ...
def round_trip_load_all(
    stream: StreamTextType,
    version: Optional[VersionType] = ...,
    preserve_quotes: Optional[bool] = ...,
) -> Any: ...
def emit(
    events: Any,
    stream: Optional[StreamType] = ...,
    Dumper: Any = ...,
    canonical: Optional[bool] = ...,
    indent: Union[int, None] = ...,
    width: Optional[int] = ...,
    allow_unicode: Optional[bool] = ...,
    line_break: Any = ...,
) -> Any: ...

enc: Incomplete

def serialize_all(
    nodes: Any,
    stream: Optional[StreamType] = ...,
    Dumper: Any = ...,
    canonical: Any = ...,
    indent: Optional[int] = ...,
    width: Optional[int] = ...,
    allow_unicode: Optional[bool] = ...,
    line_break: Any = ...,
    encoding: Any = ...,
    explicit_start: Optional[bool] = ...,
    explicit_end: Optional[bool] = ...,
    version: Optional[VersionType] = ...,
    tags: Any = ...,
) -> Any: ...
def serialize(
    node: Any, stream: Optional[StreamType] = ..., Dumper: Any = ..., **kwds: Any
) -> Any: ...
def dump_all(
    documents: Any,
    stream: Optional[StreamType] = ...,
    Dumper: Any = ...,
    default_style: Any = ...,
    default_flow_style: Any = ...,
    canonical: Optional[bool] = ...,
    indent: Optional[int] = ...,
    width: Optional[int] = ...,
    allow_unicode: Optional[bool] = ...,
    line_break: Any = ...,
    encoding: Any = ...,
    explicit_start: Optional[bool] = ...,
    explicit_end: Optional[bool] = ...,
    version: Any = ...,
    tags: Any = ...,
    block_seq_indent: Any = ...,
    top_level_colon_align: Any = ...,
    prefix_colon: Any = ...,
) -> Optional[str]: ...
def dump(
    data: Any,
    stream: Optional[StreamType] = ...,
    Dumper: Any = ...,
    default_style: Any = ...,
    default_flow_style: Any = ...,
    canonical: Optional[bool] = ...,
    indent: Optional[int] = ...,
    width: Optional[int] = ...,
    allow_unicode: Optional[bool] = ...,
    line_break: Any = ...,
    encoding: Any = ...,
    explicit_start: Optional[bool] = ...,
    explicit_end: Optional[bool] = ...,
    version: Optional[VersionType] = ...,
    tags: Any = ...,
    block_seq_indent: Any = ...,
) -> Optional[str]: ...
def safe_dump_all(
    documents: Any, stream: Optional[StreamType] = ..., **kwds: Any
) -> Optional[str]: ...
def safe_dump(
    data: Any, stream: Optional[StreamType] = ..., **kwds: Any
) -> Optional[str]: ...
def round_trip_dump(
    data: Any,
    stream: Optional[StreamType] = ...,
    Dumper: Any = ...,
    default_style: Any = ...,
    default_flow_style: Any = ...,
    canonical: Optional[bool] = ...,
    indent: Optional[int] = ...,
    width: Optional[int] = ...,
    allow_unicode: Optional[bool] = ...,
    line_break: Any = ...,
    encoding: Any = ...,
    explicit_start: Optional[bool] = ...,
    explicit_end: Optional[bool] = ...,
    version: Optional[VersionType] = ...,
    tags: Any = ...,
    block_seq_indent: Any = ...,
    top_level_colon_align: Any = ...,
    prefix_colon: Any = ...,
) -> Optional[str]: ...
def add_implicit_resolver(
    tag: Any,
    regexp: Any,
    first: Any = ...,
    Loader: Any = ...,
    Dumper: Any = ...,
    resolver: Any = ...,
) -> None: ...
def add_path_resolver(
    tag: Any,
    path: Any,
    kind: Any = ...,
    Loader: Any = ...,
    Dumper: Any = ...,
    resolver: Any = ...,
) -> None: ...
def add_constructor(
    tag: Any, object_constructor: Any, Loader: Any = ..., constructor: Any = ...
) -> None: ...
def add_multi_constructor(
    tag_prefix: Any, multi_constructor: Any, Loader: Any = ..., constructor: Any = ...
) -> None: ...
def add_representer(
    data_type: Any, object_representer: Any, Dumper: Any = ..., representer: Any = ...
) -> None: ...
def add_multi_representer(
    data_type: Any, multi_representer: Any, Dumper: Any = ..., representer: Any = ...
) -> None: ...

class YAMLObjectMetaclass(type):
    def __init__(cls, name: Any, bases: Any, kwds: Any) -> None: ...

class YAMLObject:
    yaml_constructor = Constructor
    yaml_representer = Representer
    yaml_tag: Any
    yaml_flow_style: Any
    @classmethod
    def from_yaml(cls, constructor: Any, node: Any) -> Any: ...
    @classmethod
    def to_yaml(cls, representer: Any, data: Any) -> Any: ...
