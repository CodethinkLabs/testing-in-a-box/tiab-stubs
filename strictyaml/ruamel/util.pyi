from .compat import (
    StreamTextType as StreamTextType,
    binary_type as binary_type,
    text_type as text_type,
)
from _typeshed import Incomplete
from typing import Any

class LazyEval:
    def __init__(self, func: Any, *args: Any, **kwargs: Any) -> None: ...
    def __getattribute__(self, name: Any) -> Any: ...
    def __setattr__(self, name: Any, value: Any) -> None: ...

RegExp: Incomplete

def load_yaml_guess_indent(stream: StreamTextType, **kw: Any) -> Any: ...
def configobj_walker(cfg: Any) -> Any: ...
