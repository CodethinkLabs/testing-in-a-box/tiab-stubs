from _typeshed import Incomplete
from collections.abc import Mapping, MutableSet, Set, Sized
from strictyaml.ruamel.compat import MutableSliceableSequence, ordereddict
from typing import Any, Iterator, Optional

comment_attrib: str
merge_attrib: str

def raise_immutable(cls: Any, *args: Any, **kwargs: Any) -> None: ...

class Comment:
    attrib: str
    comment: Incomplete
    def __init__(self) -> None: ...
    @property
    def items(self) -> Any: ...
    @property
    def end(self) -> Any: ...
    @end.setter
    def end(self, value: Any) -> None: ...
    @property
    def start(self) -> Any: ...
    @start.setter
    def start(self, value: Any) -> None: ...

class Format:
    attrib: str
    def __init__(self) -> None: ...
    def set_flow_style(self) -> None: ...
    def set_block_style(self) -> None: ...
    def flow_style(self, default: Optional[Any] = ...) -> Any: ...

class LineCol:
    attrib: str
    line: Incomplete
    col: Incomplete
    data: Incomplete
    def __init__(self) -> None: ...
    def add_kv_line_col(self, key: Any, data: Any) -> None: ...
    def key(self, k: Any) -> Any: ...
    def value(self, k: Any) -> Any: ...
    def item(self, idx: Any) -> Any: ...
    def add_idx_line_col(self, key: Any, data: Any) -> None: ...

class Tag:
    attrib: str
    value: Incomplete
    def __init__(self) -> None: ...

class CommentedBase:
    @property
    def ca(self) -> Any: ...
    def yaml_end_comment_extend(self, comment: Any, clear: bool = ...) -> None: ...
    def yaml_key_comment_extend(
        self, key: Any, comment: Any, clear: bool = ...
    ) -> None: ...
    def yaml_value_comment_extend(
        self, key: Any, comment: Any, clear: bool = ...
    ) -> None: ...
    def yaml_set_start_comment(self, comment: Any, indent: Any = ...) -> None: ...
    def yaml_set_comment_before_after_key(
        self,
        key: Any,
        before: Any = ...,
        indent: Any = ...,
        after: Any = ...,
        after_indent: Any = ...,
    ) -> None: ...
    @property
    def fa(self) -> Any: ...
    def yaml_add_eol_comment(
        self, comment: Any, key: Optional[Any] = ..., column: Optional[Any] = ...
    ) -> None: ...
    @property
    def lc(self) -> Any: ...
    @property
    def anchor(self) -> Any: ...
    def yaml_anchor(self) -> Any: ...
    def yaml_set_anchor(self, value: Any, always_dump: bool = ...) -> None: ...
    @property
    def tag(self) -> Any: ...
    def yaml_set_tag(self, value: Any) -> None: ...
    def copy_attributes(self, t: Any, memo: Any = ...) -> None: ...

class CommentedSeq(MutableSliceableSequence, list, CommentedBase):  # type: ignore
    def __init__(self, *args: Any, **kw: Any) -> None: ...
    def __getsingleitem__(self, idx: Any) -> Any: ...
    def __setsingleitem__(self, idx: Any, value: Any) -> None: ...
    def __delsingleitem__(self, idx: Any = ...) -> Any: ...
    def __len__(self) -> int: ...
    def insert(self, idx: Any, val: Any) -> None: ...
    def extend(self, val: Any) -> None: ...
    def __eq__(self, other: Any) -> bool: ...
    def __deepcopy__(self, memo: Any) -> Any: ...
    def __add__(self, other: Any) -> Any: ...
    def sort(self, key: Any = ..., reverse: bool = ...) -> None: ...

class CommentedKeySeq(tuple, CommentedBase): ...  # type: ignore

class CommentedMapView(Sized):
    def __init__(self, mapping: Any) -> None: ...
    def __len__(self) -> int: ...

class CommentedMapKeysView(CommentedMapView, Set):  # type: ignore
    def __contains__(self, key: Any) -> Any: ...
    def __iter__(self) -> Any: ...

class CommentedMapItemsView(CommentedMapView, Set):  # type: ignore
    def __contains__(self, item: Any) -> Any: ...
    def __iter__(self) -> Any: ...

class CommentedMapValuesView(CommentedMapView):
    def __contains__(self, value: Any) -> Any: ...
    def __iter__(self) -> Any: ...

class CommentedMap(ordereddict, CommentedBase):
    def __init__(self, *args: Any, **kw: Any) -> None: ...
    def update(self, *vals: Any, **kw: Any) -> None: ...
    def insert(
        self, pos: Any, key: Any, value: Any, comment: Optional[Any] = ...
    ) -> None: ...
    def mlget(self, key: Any, default: Any = ..., list_ok: Any = ...) -> Any: ...
    def __getitem__(self, key: Any) -> Any: ...
    def __setitem__(self, key: Any, value: Any) -> None: ...
    def __contains__(self, key: Any) -> bool: ...
    def get(self, key: Any, default: Any = ...) -> Any: ...
    def non_merged_items(self) -> Any: ...
    def __delitem__(self, key: Any) -> None: ...
    def __iter__(self) -> Any: ...
    def __len__(self) -> int: ...
    def __eq__(self, other: Any) -> bool: ...
    def keys(self) -> Any: ...
    def values(self) -> Any: ...
    def items(self) -> Any: ...
    @property
    def merge(self) -> Any: ...
    def copy(self) -> Any: ...
    def add_referent(self, cm: Any) -> None: ...
    def add_yaml_merge(self, value: Any) -> None: ...
    def update_key_value(self, key: Any) -> None: ...
    def __deepcopy__(self, memo: Any) -> Any: ...

class CommentedKeyMap(CommentedBase, Mapping):  # type: ignore
    def __init__(self, *args: Any, **kw: Any) -> None: ...
    __delitem__ = raise_immutable
    __setitem__ = raise_immutable
    clear = raise_immutable
    pop = raise_immutable
    popitem = raise_immutable
    setdefault = raise_immutable
    update = raise_immutable
    def __getitem__(self, index: Any) -> Any: ...
    def __iter__(self) -> Iterator[Any]: ...
    def __len__(self) -> int: ...
    def __hash__(self) -> Any: ...
    @classmethod
    def fromkeys(keys: Any, v: Any = ...) -> Any: ...

class CommentedOrderedMap(CommentedMap): ...

class CommentedSet(MutableSet, CommentedBase):  # type: ignore
    odict: Incomplete
    def __init__(self, values: Any = ...) -> None: ...
    def add(self, value: Any) -> None: ...
    def discard(self, value: Any) -> None: ...
    def __contains__(self, x: Any) -> Any: ...
    def __iter__(self) -> Any: ...
    def __len__(self) -> int: ...

class TaggedScalar(CommentedBase):
    value: Incomplete
    style: Incomplete
    def __init__(self, value: Any = ..., style: Any = ..., tag: Any = ...) -> None: ...
