from _typeshed import Incomplete

anchor_attrib: str

class Anchor:
    attrib = anchor_attrib
    value: Incomplete
    always_dump: bool
    def __init__(self) -> None: ...
