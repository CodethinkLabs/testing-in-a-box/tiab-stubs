from .compat import string_types as string_types
from _typeshed import Incomplete
from typing import Any

class Node:
    tag: Incomplete
    value: Incomplete
    start_mark: Incomplete
    end_mark: Incomplete
    comment: Incomplete
    anchor: Incomplete
    def __init__(
        self,
        tag: Any,
        value: Any,
        start_mark: Any,
        end_mark: Any,
        comment: Any = ...,
        anchor: Any = ...,
    ) -> None: ...
    def dump(self, indent: int = ...) -> None: ...

class ScalarNode(Node):
    id: str
    style: Incomplete
    def __init__(
        self,
        tag: Any,
        value: Any,
        start_mark: Any = ...,
        end_mark: Any = ...,
        style: Any = ...,
        comment: Any = ...,
        anchor: Any = ...,
    ) -> None: ...

class CollectionNode(Node):
    flow_style: Incomplete
    anchor: Incomplete
    def __init__(
        self,
        tag: Any,
        value: Any,
        start_mark: Any = ...,
        end_mark: Any = ...,
        flow_style: Any = ...,
        comment: Any = ...,
        anchor: Any = ...,
    ) -> None: ...

class SequenceNode(CollectionNode):
    id: str

class MappingNode(CollectionNode):
    id: str
    merge: Incomplete
    def __init__(
        self,
        tag: Any,
        value: Any,
        start_mark: Any = ...,
        end_mark: Any = ...,
        flow_style: Any = ...,
        comment: Any = ...,
        anchor: Any = ...,
    ) -> None: ...
