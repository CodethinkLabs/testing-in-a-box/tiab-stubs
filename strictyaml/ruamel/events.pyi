from _typeshed import Incomplete
from typing import Any, Optional

def CommentCheck() -> None: ...

class Event:
    start_mark: Incomplete
    end_mark: Incomplete
    comment: Incomplete
    def __init__(
        self, start_mark: Any = ..., end_mark: Any = ..., comment: Any = ...
    ) -> None: ...

class NodeEvent(Event):
    anchor: Incomplete
    def __init__(
        self,
        anchor: Any,
        start_mark: Any = ...,
        end_mark: Any = ...,
        comment: Any = ...,
    ) -> None: ...

class CollectionStartEvent(NodeEvent):
    tag: Incomplete
    implicit: Incomplete
    flow_style: Incomplete
    nr_items: Incomplete
    def __init__(
        self,
        anchor: Any,
        tag: Any,
        implicit: Any,
        start_mark: Any = ...,
        end_mark: Any = ...,
        flow_style: Any = ...,
        comment: Any = ...,
        nr_items: Optional[int] = ...,
    ) -> None: ...

class CollectionEndEvent(Event): ...

class StreamStartEvent(Event):
    encoding: Incomplete
    def __init__(
        self,
        start_mark: Any = ...,
        end_mark: Any = ...,
        encoding: Any = ...,
        comment: Any = ...,
    ) -> None: ...

class StreamEndEvent(Event): ...

class DocumentStartEvent(Event):
    explicit: Incomplete
    version: Incomplete
    tags: Incomplete
    def __init__(
        self,
        start_mark: Any = ...,
        end_mark: Any = ...,
        explicit: Any = ...,
        version: Any = ...,
        tags: Any = ...,
        comment: Any = ...,
    ) -> None: ...

class DocumentEndEvent(Event):
    explicit: Incomplete
    def __init__(
        self,
        start_mark: Any = ...,
        end_mark: Any = ...,
        explicit: Any = ...,
        comment: Any = ...,
    ) -> None: ...

class AliasEvent(NodeEvent): ...

class ScalarEvent(NodeEvent):
    tag: Incomplete
    implicit: Incomplete
    value: Incomplete
    style: Incomplete
    def __init__(
        self,
        anchor: Any,
        tag: Any,
        implicit: Any,
        value: Any,
        start_mark: Any = ...,
        end_mark: Any = ...,
        style: Any = ...,
        comment: Any = ...,
    ) -> None: ...

class SequenceStartEvent(CollectionStartEvent): ...
class SequenceEndEvent(CollectionEndEvent): ...
class MappingStartEvent(CollectionStartEvent): ...
class MappingEndEvent(CollectionEndEvent): ...
