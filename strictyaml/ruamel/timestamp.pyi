import datetime
from _typeshed import Incomplete
from typing import Any

class TimeStamp(datetime.datetime):
    def __init__(self, *args: Any, **kw: Any) -> None: ...
    def __new__(cls, *args: Any, **kw: Any) -> Any: ...
    def __deepcopy__(self, memo: Any) -> Any: ...
    def replace(
        self,
        year: Incomplete | None = ...,
        month: Incomplete | None = ...,
        day: Incomplete | None = ...,
        hour: Incomplete | None = ...,
        minute: Incomplete | None = ...,
        second: Incomplete | None = ...,
        microsecond: Incomplete | None = ...,
        tzinfo: bool = ...,
        fold: Incomplete | None = ...,
    ): ...
