from _ruamel_yaml import CEmitter, CParser  # type: ignore
from strictyaml.ruamel.compat import StreamTextType, VersionType
from strictyaml.ruamel.constructor import BaseConstructor, Constructor, SafeConstructor
from strictyaml.ruamel.representer import BaseRepresenter, Representer, SafeRepresenter
from strictyaml.ruamel.resolver import BaseResolver, Resolver
from typing import Any, Optional

class CBaseLoader(CParser, BaseConstructor, BaseResolver):
    def __init__(
        self,
        stream: StreamTextType,
        version: Optional[VersionType] = ...,
        preserve_quotes: Optional[bool] = ...,
    ) -> None: ...

class CSafeLoader(CParser, SafeConstructor, Resolver):
    def __init__(
        self,
        stream: StreamTextType,
        version: Optional[VersionType] = ...,
        preserve_quotes: Optional[bool] = ...,
    ) -> None: ...

class CLoader(CParser, Constructor, Resolver):
    def __init__(
        self,
        stream: StreamTextType,
        version: Optional[VersionType] = ...,
        preserve_quotes: Optional[bool] = ...,
    ) -> None: ...

class CBaseDumper(CEmitter, BaseRepresenter, BaseResolver):
    def __init__(
        self,
        stream: Any,
        default_style: Any = ...,
        default_flow_style: Any = ...,
        canonical: Optional[bool] = ...,
        indent: Optional[int] = ...,
        width: Optional[int] = ...,
        allow_unicode: Optional[bool] = ...,
        line_break: Any = ...,
        encoding: Any = ...,
        explicit_start: Optional[bool] = ...,
        explicit_end: Optional[bool] = ...,
        version: Any = ...,
        tags: Any = ...,
        block_seq_indent: Any = ...,
        top_level_colon_align: Any = ...,
        prefix_colon: Any = ...,
    ) -> None: ...

class CSafeDumper(CEmitter, SafeRepresenter, Resolver):
    def __init__(
        self,
        stream: Any,
        default_style: Any = ...,
        default_flow_style: Any = ...,
        canonical: Optional[bool] = ...,
        indent: Optional[int] = ...,
        width: Optional[int] = ...,
        allow_unicode: Optional[bool] = ...,
        line_break: Any = ...,
        encoding: Any = ...,
        explicit_start: Optional[bool] = ...,
        explicit_end: Optional[bool] = ...,
        version: Any = ...,
        tags: Any = ...,
        block_seq_indent: Any = ...,
        top_level_colon_align: Any = ...,
        prefix_colon: Any = ...,
    ) -> None: ...

class CDumper(CEmitter, Representer, Resolver):
    def __init__(
        self,
        stream: Any,
        default_style: Any = ...,
        default_flow_style: Any = ...,
        canonical: Optional[bool] = ...,
        indent: Optional[int] = ...,
        width: Optional[int] = ...,
        allow_unicode: Optional[bool] = ...,
        line_break: Any = ...,
        encoding: Any = ...,
        explicit_start: Optional[bool] = ...,
        explicit_end: Optional[bool] = ...,
        version: Any = ...,
        tags: Any = ...,
        block_seq_indent: Any = ...,
        top_level_colon_align: Any = ...,
        prefix_colon: Any = ...,
    ) -> None: ...
