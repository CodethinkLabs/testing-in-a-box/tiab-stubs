from _typeshed import Incomplete
from strictyaml import exceptions as exceptions, utils as utils
from strictyaml.any_validator import Any as Any
from strictyaml.ruamel.comments import (
    CommentedMap as CommentedMap,
    CommentedSeq as CommentedSeq,
)
from strictyaml.ruamel.compat import PY2 as PY2
from strictyaml.ruamel.composer import Composer as Composer
from strictyaml.ruamel.constructor import (
    ConstructorError as ConstructorError,
    RoundTripConstructor as RoundTripConstructor,
)
from strictyaml.ruamel.nodes import MappingNode as MappingNode
from strictyaml.ruamel.parser import RoundTripParser as RoundTripParser
from strictyaml.ruamel.reader import Reader as Reader
from strictyaml.ruamel.resolver import VersionedResolver as VersionedResolver
from strictyaml.ruamel.scanner import RoundTripScanner as RoundTripScanner
from strictyaml.representation import YAML
from strictyaml.validators import Validator
from strictyaml.yamllocation import YAMLChunk as YAMLChunk
from typing import Optional as _Optional

class StrictYAMLConstructor(RoundTripConstructor):
    yaml_constructors: Incomplete
    def construct_mapping(self, node, maptyp, deep: bool = ...) -> None: ...  # type: ignore

class StrictYAMLScanner(RoundTripScanner):
    def check_token(self, *choices): ...

class StrictYAMLLoader(
    Reader,
    StrictYAMLScanner,
    RoundTripParser,
    Composer,
    StrictYAMLConstructor,
    VersionedResolver,
):
    def __init__(
        self,
        stream,
        version: Incomplete | None = ...,
        preserve_quotes: Incomplete | None = ...,
    ) -> None: ...

def as_document(
    data: Incomplete, schema: _Optional[Validator] = ..., label: str = ...
): ...
def generic_load(
    yaml_string: str,
    schema: _Optional[Validator] = ...,
    label: str = ...,
    allow_flow_style: bool = ...,
): ...
def dirty_load(
    yaml_string: str,
    schema: _Optional[Validator] = ...,
    label: str = ...,
    allow_flow_style: bool = ...,
): ...
def load(
    yaml_string: str, schema: _Optional[Validator] = ..., label: str = ...
) -> YAML: ...
