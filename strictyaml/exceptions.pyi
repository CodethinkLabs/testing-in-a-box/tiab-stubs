from _typeshed import Incomplete
from strictyaml.ruamel import dump as dump
from strictyaml.ruamel.dumper import RoundTripDumper as RoundTripDumper
from strictyaml.ruamel.error import (
    MarkedYAMLError as MarkedYAMLError,
    StringMark as StringMark,
)

class StrictYAMLError(MarkedYAMLError): ...
class InvalidValidatorError(StrictYAMLError): ...
class CannotBuildDocumentFromInvalidData(StrictYAMLError): ...
class CannotBuildDocumentsFromEmptyDictOrList(StrictYAMLError): ...
class YAMLSerializationError(StrictYAMLError): ...
class InvalidOptionalDefault(YAMLSerializationError): ...

class YAMLValidationError(StrictYAMLError):
    context: Incomplete
    problem: Incomplete
    note: Incomplete
    def __init__(self, context, problem, chunk) -> None: ...
    @property
    def context_mark(self): ...
    @property
    def problem_mark(self): ...

class DisallowedToken(StrictYAMLError):
    MESSAGE: str

class TagTokenDisallowed(DisallowedToken):
    MESSAGE: str

class FlowMappingDisallowed(DisallowedToken):
    MESSAGE: str

class AnchorTokenDisallowed(DisallowedToken):
    MESSAGE: str

class DuplicateKeysDisallowed(DisallowedToken):
    MESSAGE: str

class InconsistentIndentationDisallowed(DisallowedToken):
    MESSAGE: str

def raise_type_error(yaml_object, to_type, alternatives) -> None: ...
