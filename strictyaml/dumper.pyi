from _typeshed import Incomplete
from strictyaml.ruamel.compat import StreamType
from strictyaml.ruamel.emitter import Emitter as Emitter
from strictyaml.ruamel.representer import RoundTripRepresenter as RoundTripRepresenter
from strictyaml.ruamel.resolver import BaseResolver as BaseResolver
from strictyaml.ruamel.scalarstring import ScalarString as ScalarString
from strictyaml.ruamel.serializer import Serializer as Serializer
from typing import Any, Union

class StrictYAMLResolver(BaseResolver):
    def __init__(
        self, version: Incomplete | None = ..., loader: Incomplete | None = ...
    ) -> None: ...

class StrictYAMLDumper(Emitter, Serializer, RoundTripRepresenter, StrictYAMLResolver):
    def __init__(
        self,
        stream: Any,
        default_style: StreamType = ...,
        default_flow_style: Any = ...,
        canonical: bool = ...,
        indent: Union[None, int] = ...,
        width: Union[None, int] = ...,
        allow_unicode: bool = ...,
        line_break: Any = ...,
        encoding: Any = ...,
        explicit_start: Union[None, bool] = ...,
        explicit_end: Union[None, bool] = ...,
        version: Any = ...,
        tags: Any = ...,
        block_seq_indent: Any = ...,
        top_level_colon_align: Any = ...,
        prefix_colon: Any = ...,
    ) -> None: ...
